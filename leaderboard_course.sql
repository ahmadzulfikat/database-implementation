-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2020 at 06:30 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `leaderboard_course`
--

-- --------------------------------------------------------

--
-- Table structure for table `game_tbl`
--

CREATE TABLE `game_tbl` (
  `game_id` int(11) NOT NULL,
  `game_name` varchar(100) NOT NULL,
  `game_level` int(11) NOT NULL,
  `game_genre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `game_tbl`
--

INSERT INTO `game_tbl` (`game_id`, `game_name`, `game_level`, `game_genre`) VALUES
(1, 'final fanatsy 69', 2, 'gelud'),
(2, 'mario kart', 3, 'sport'),
(3, 'feeding frenzy', 6, 'adventure'),
(4, 'gta sanandreas', 9, 'action'),
(7, 'uwu', 69, 'owo');

-- --------------------------------------------------------

--
-- Table structure for table `leaderboard_tbl`
--

CREATE TABLE `leaderboard_tbl` (
  `leaderboard_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `game_level` int(11) NOT NULL,
  `score_game` int(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `leaderboard_tbl`
--

INSERT INTO `leaderboard_tbl` (`leaderboard_id`, `user_id`, `game_id`, `game_level`, `score_game`, `start_time`, `end_time`) VALUES
(1, 1, 4, 0, 78, '2020-10-22 12:26:44', '2020-10-22 12:27:56'),
(2, 2, 3, 0, 98, '2020-10-22 12:26:44', '2020-10-22 12:27:56'),
(3, 3, 2, 0, 45, '2020-10-22 12:26:44', '2020-10-22 12:27:56'),
(4, 4, 1, 0, 76, '2020-10-22 12:26:44', '2020-10-22 12:27:56'),
(5, 5, 2, 0, 87, '2020-10-22 12:26:44', '2020-10-22 12:27:56'),
(6, 1, 3, 0, 90, '2020-10-22 12:26:44', '2020-10-22 12:27:56'),
(7, 2, 4, 0, 12, '2020-10-22 12:26:44', '2020-10-22 12:27:56'),
(8, 3, 3, 0, 100, '2020-10-22 12:26:44', '2020-10-22 12:27:56'),
(9, 7, 3, 0, 999, '2020-10-22 13:43:47', '2020-10-22 13:43:49'),
(10, 7, 1, 0, 69, '2020-10-22 13:41:47', '2020-10-22 13:49:49'),
(11, 7, 3, 0, 80, '2020-10-28 17:02:57', '2020-10-28 17:02:57'),
(12, 7, 1, 0, 67, '2020-10-28 17:04:02', '2020-10-27 17:04:02'),
(14, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 7, 1, 123, 123213, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 7, 2, 2, 1000, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 1, 1, 1, 1, '2020-10-29 00:00:00', '2020-10-29 00:00:00'),
(19, 2, 2, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 7, 4, 90, 9000000, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 7, 4, 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 7, 3, 1, 9000, '2020-10-30 00:00:00', '2020-10-31 00:00:00'),
(23, 8, 7, 1, 1000, '2020-10-31 00:00:00', '2020-10-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name_user` varchar(100) NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`user_id`, `username`, `email`, `name_user`, `password`, `token`) VALUES
(1, 'ahmad', 'ahmad@gmail.com', 'Ah_mad_Man360', 'ahmad', 'asdsadsadsadsad'),
(2, 'sephiroth', 'sephiroth@gmail.com', 'i_kill_aerith_H3h3h3', 'sephiroth', 'qweqeqweweqw'),
(3, 'zulfikar', 'zulfikar@gmail.com', 'iZulz222', 'zulfikar', 'xzczxcxcxzczxc'),
(4, 'gacha', 'gacha@gmail.com', 'gacha_ampas_sekali', 'gacha', 'rtytytrytytryr'),
(5, 'admin', 'admin', 'admin_hehehehe', 'admin', 'fhgfhghgfgfhgfhfgh'),
(7, 'test', 'test@gmail.com', 'test', '098f6bcd4621d373cade4e832627b4f6', 'f1bed054f08ee7662c87cd8a7023fb4a'),
(8, 'wew', 'wew', 'wew', '3847820138564525205299f1f444c5ec', 'bdb056a83ba7f80a52dc507f8b334bb2'),
(9, 'zak', 'zak@email.com', 'ZAcK_afro', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'c548dfc5757653c0f2a1696661730df3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `game_tbl`
--
ALTER TABLE `game_tbl`
  ADD PRIMARY KEY (`game_id`);

--
-- Indexes for table `leaderboard_tbl`
--
ALTER TABLE `leaderboard_tbl`
  ADD PRIMARY KEY (`leaderboard_id`),
  ADD KEY `user_id_relation_view` (`user_id`),
  ADD KEY `game_id_relation_view` (`game_id`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `game_tbl`
--
ALTER TABLE `game_tbl`
  MODIFY `game_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `leaderboard_tbl`
--
ALTER TABLE `leaderboard_tbl`
  MODIFY `leaderboard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `leaderboard_tbl`
--
ALTER TABLE `leaderboard_tbl`
  ADD CONSTRAINT `game_id_relation_view` FOREIGN KEY (`game_id`) REFERENCES `game_tbl` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id_relation_view` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
